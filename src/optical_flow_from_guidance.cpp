#include "ros/ros.h"
#include "std_msgs/Int8.h"
#include "optical_flow_node/optical_flow.h"

// GuidanceNode.cpp headers... not sure if all are needed
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/LaserScan.h>

// lkdemo.cpp header functions... not sure if all are needed
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <ctype.h>
using namespace cv;
using namespace std;


// here's the function that implements optical flow w/ guidance camera data
void imgCallback(const sensor_msgs::ImageConstPtr& img)
{
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);

    const int MAX_COUNT = 500;
    bool needToInit = false;
    Point2f point;
    bool addRemovePt = false;

    // prob don't need "cap" for computer's camera since we're using Guidance camera
    /*
    if( argc == 1 || (argc == 2 && strlen(argv[1]) == 1 && isdigit(argv[1][0])))
        cap.open(argc == 2 ? argv[1][0] - '0' : 0);
    else if( argc == 2 )
        cap.open(argv[1]);

    if( !cap.isOpened() )
    {
        cout << "Could not initialize capturing...\n";
        return 0;
    } */

    namedWindow( "Optical flow from Guidance", 1 );

    Mat gray, prevGray /*, image, frame */;
    vector<Point2f> points[2];

    for(;;)
    {
        /*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   */
        //PROBLEMS HERE WITH CONVERTING ROS IMAGE FORMAT TO OPENCV MAT FORMAT
        /*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   */

        CvImagePtr toCvCopy(const sensor_msgs::ImageConstPtr& img, const std::string& gray);
        //gray = img->data;
        if( gray.empty() )
            break;

        // prob don't need this b/c Guidance img is already gray
        /* frame.copyTo(image);
        cvtColor(image, gray, COLOR_BGR2GRAY); */

        // prob don't need this b/c won't need night mode
        /*if( nightMode )
            image = Scalar::all(0);*/

        if( needToInit )
        {
            // automatic initialization
            goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
            cornerSubPix(gray, points[1], subPixWinSize, Size(-1,-1), termcrit);
            addRemovePt = false;
        }
        else if( !points[0].empty() )
        {
            vector<uchar> status;
            vector<float> err;
            if(prevGray.empty())
                gray.copyTo(prevGray);
            calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize,
                                 3, termcrit, 0, 0.001);
            size_t i, k;
            for( i = k = 0; i < points[1].size(); i++ )
            {
                if( addRemovePt )
                {
                    if( norm(point - points[1][i]) <= 5 )
                    {
                        addRemovePt = false;
                        continue;
                    }
                }

                if( !status[i] )
                    continue;

                points[1][k++] = points[1][i];
                circle( gray, points[1][i], 3, Scalar(0,255,0), -1, 8);
            }
            points[1].resize(k);
        }

        if( addRemovePt && points[1].size() < (size_t)MAX_COUNT )
        {
            vector<Point2f> tmp;
            tmp.push_back(point);
            cornerSubPix( gray, tmp, winSize, cvSize(-1,-1), termcrit);
            points[1].push_back(tmp[0]);
            addRemovePt = false;
        }

        needToInit = false;
        imshow("Optical flow from Guidance", gray);

        // not going to accept keyboard commands
        /*
        char c = (char)waitKey(10);
        if( c == 27 )
            break;
        switch( c )
        {
        case 'r':
            needToInit = true;
            break;
        case 'c':
            points[0].clear();
            points[1].clear();
            break;
        case 'n':
            nightMode = !nightMode;
            break;  
        } */

        std::swap(points[1], points[0]);
        cv::swap(prevGray, gray);
    }

   // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
   /* insert code that calculates dX,dY from track lengths */
   // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   //std_msgs::Int8 dx, dy;
   //dx == 0; dy == 0;

   // code to publish dx,dy so Shan's node can subscribe to it.
   // 
   ros::NodeHandle nh;
   ros::Publisher pub = nh.advertise<optical_flow_node::optical_flow>("optical_flow_topic", 10);
   optical_flow_node::optical_flow opt_flow;

   opt_flow.dx = 0;
   opt_flow.dy = 0;

   pub.publish(opt_flow);

}





// the function that subscribes to guidance camera 
int main(int argc, char **argv)
{
  ros::init(argc, argv, "optical_flow_node");
    // creates a node named optical_flow_node
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/guidance/left_image", 100, imgCallback);
    // "100" refers to number of messages queued before starting to discard

  ros::spin();
  return 0;
}
